#!/usr/bin/env node

const manager = require('commander');
const { prompt } = require('inquirer');

const {
	addKey,
	getKey,
	getAllKeys,
	updateKey,
	deleteKey
} = require('./models/matchkey');

const questions = [
	{
		type: 'input',
		name: 'abbrev',
		message: 'Enter abbreviation ...'
	},
	{
		type: 'input',
		name: 'matchkey',
		message: 'Enter matchkey ...'
	}
];

manager
	.version('1.0.0')
	.description('Command line utility for Betwrench app administration');

manager
	.command('addKey')
	.alias('ak')
	.description('Add a new match key')
	.action(() => {
		prompt(questions).then(answers => 
			addKey(answers));
	});

manager
	.command('getKey <key_name>')
	.alias('gk')
	.description('Get match key')
	.action(key_name => getKey(key_name));

manager
	.command('getAllKeys')
	.alias('gk')
	.description('Get all match keys')
	.action(() => getAllKeys());

manager
	.command('updateKey <_id>')
	.alias('uk')
	.description('Update match key')
	.action(_id => {
		prompt(questions).then((answers) => 
			updateKey(_id, answers));
	});

manager
	.command('deleteKey <_id>')
	.alias('dk')
	.description('Delete match key')
	.action(_id => deleteKey(_id));

if(!process.argv.slice(2).length || !/[arudl]/.test(process.argv.slice(2))) {
	manager.outputHelp();
	process.exit();
}

manager.parse(process.argv);