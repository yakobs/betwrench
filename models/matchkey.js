const mongoose = require('mongoose');
const assert = require('assert');
mongoose.Promise = global.Promise;

const db = mongoose.connect('mongodb://localhost:27017/bet-wrench', {
	useNewUrlParser: true
});

const matchKeySchema = mongoose.Schema({
	abbrev: { type: String },
	matchkey: {type: String }
});

const MatchKey = mongoose.model('MatchKey', matchKeySchema);

const addKey = (matchKey) => {
	MatchKey.create(matchKey, (err) => {
		assert.equal(null, err);
		console.info('New match key added');
		mongoose.connection.close();
	});
};

const getKey = (key_name) => {
	MatchKey.find({ abbrev: key_name })
	.exec((err, key) => {
		assert.equal(null, err);
		console.info(key);
		console.info(`${key.length} matches`);
		mongoose.connection.close();
	});
};

const updateKey = (_id, matchKey) => {
	MatchKey.update({ _id }, matchKey)
	.exec((err, status) => {
		assert.equal(null, err);
		console.info('Updated successfully');
		mongoose.connection.close();
	});
};

const deleteKey = (_id) => {
	MatchKey.remove({ _id })
	.exec((err, status) => {
		assert.equal(null, err);
		console.info('Deleted seccessfully');
		mongoose.connection.close();
	});
};

const getAllKeys = () => {
	MatchKey.find({})
	.exec((err, matchKey) => {
		assert.equal(null, err);
		console.info(matchKey);
		console.info(`${matchKey.length} match keys`);
		mongoose.connection.close();
	});
};

module.exports = {
	addKey,
	getKey,
	getAllKeys,
	updateKey,
	deleteKey
};